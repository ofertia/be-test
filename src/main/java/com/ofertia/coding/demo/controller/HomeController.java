package com.ofertia.coding.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class HomeController {

    private static final String WELCOME_TEXT = "Hello, Ofertia!";

	@RequestMapping("/")
	public @ResponseBody String greeting() {
        log.info(WELCOME_TEXT);
		return WELCOME_TEXT;
	}

}
